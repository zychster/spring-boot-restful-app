package com.example.demo.controllers;

import com.example.demo.Message;
import com.example.demo.MessageRepository;
import com.example.demo.resources.EmailResource;
import com.example.demo.services.MailerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ContactFormsController {

    private MessageRepository messageRepository;

    private MailerService mailerService;

    @Autowired
    public ContactFormsController(MessageRepository messageRepository, MailerService mailerService) {
        this.messageRepository = messageRepository;
        this.mailerService = mailerService;
    }

    @RequestMapping(value = "/message", method = RequestMethod.POST)
    public void sayHello(@RequestBody Message message) {
        messageRepository.save(message);
    }

    @RequestMapping(value = "/mail", method = RequestMethod.POST)
    public ResponseEntity<String> sendEmail(@RequestBody EmailResource emailResource) {
        mailerService.sendMessage(emailResource.getFromEmail(), "majkmarta@gmail.com",
                emailResource.getSubject(), emailResource.getMessageBody());

        return new ResponseEntity<String>("Wysłano maila", HttpStatus.OK);
    }
}
