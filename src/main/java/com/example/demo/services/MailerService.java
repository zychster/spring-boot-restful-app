package com.example.demo.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Service
public class MailerService {

    @Autowired
    private JavaMailSender javaMailSender;

     public void sendMessage(String emailFrom, String emailTo, String subject,
                             String messageText){
         MimeMessage mimeMessage = javaMailSender.createMimeMessage();
         MimeMessageHelper message = new MimeMessageHelper(mimeMessage);

         try {
             message.setFrom(emailFrom);
             message.setTo(emailTo);
             message.setSubject(subject);
             message.setText(messageText);

             javaMailSender.send(mimeMessage);
         } catch (MessagingException e) {
             e.printStackTrace();
         }
     }
}
